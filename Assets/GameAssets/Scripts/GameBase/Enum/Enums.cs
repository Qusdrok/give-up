﻿namespace GameAssets.Scripts.GameBase.Enum
{
    public enum CharacterState
    {
        Idle = 0,
        Move = 1,
        Attack = 2,
        Death = 3,
        NonAttack = 4
    }
    
    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        PreparePlay,
        Playing,
        Lose,
        Win
    }

    public enum UIType
    {
        None,
        UILobby,
        UILoading,
        UIPlay,
        UIGameStart,
        UIGameComplete,
        UIGameOver,
        UIStore,
        UIShop
    }
}