﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class AnimatorHelper
    {
        public static async Task<float> ClipLength(this Animator animator)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.125f));
            var clip = animator.GetCurrentAnimatorClipInfo(0);
            return clip[0].clip.length;
        }

        public static async Task<bool> IsPlaying(this Animator animator, string stateName = default)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.125f));
            var state = animator.GetCurrentAnimatorStateInfo(0);
            var clip = animator.GetCurrentAnimatorClipInfo(0)[0].clip;

            var clipLength = clip.length;
            var currentTime = clipLength * state.normalizedTime;

            Debug.Assert(stateName != null, nameof(stateName) + " != null");
            if (!stateName.Equals(default)) return currentTime >= clipLength && state.IsName(stateName);
            return currentTime <= clipLength;
        }

        public static async void OnCompleted(this Animator animator, Action actionOnComplete = null,
            float second = 0f, Action actionOnSecond = null)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.125f));
            var state = animator.GetCurrentAnimatorStateInfo(0);
            var clip = animator.GetCurrentAnimatorClipInfo(0)[0].clip;

            var clipLength = clip.length;
            var currentTime = clip.length * state.normalizedTime;

            Debug.Log($"START ANIMATION ===> {clip.name}");
            while (currentTime < clipLength)
            {
                if (animator == null) return;
                state = animator.GetCurrentAnimatorStateInfo(0);
                currentTime = clipLength * state.normalizedTime;

                if (currentTime >= second)
                {
                    actionOnSecond?.Invoke();
                    actionOnSecond = null;
                }

                await Task.Yield();
            }

            Debug.Log($"COMPLETE ANIMATION ===> {clip.name}");
            actionOnComplete?.Invoke();
        }

        public static void ResetNormalizedTime(this Animator animator, string stateName)
        {
            animator.Play(Animator.StringToHash(stateName), -1, 0f);
        }
    }
}