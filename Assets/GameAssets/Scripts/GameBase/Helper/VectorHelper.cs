﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class VectorHelper
    {
        public static float GetDistance(this Vector3 a, Vector3 b)
        {
            return (a - b).sqrMagnitude;
        }

        public static bool CompareDistance(this Vector3 a, Vector3 b, float distance)
        {
            return a.GetDistance(b) < distance * distance;
        }
    }
}