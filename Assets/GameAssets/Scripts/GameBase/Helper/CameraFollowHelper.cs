﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameAssets.Scripts.GameBase.Helper
{
    public class CameraFollowHelper : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private Tilemap tilemap;
        [SerializeField] private GameObject background;

        [SerializeField] private float speed;

        private float _xMin;
        private float _xMax;
        private float _yMin;
        private float _yMax;

        private void Start()
        {
            if (background != null)
            {
                Debug.Assert(Camera.main != null, "Camera.main != null");
                var height = Camera.main.orthographicSize / 1.25f;
                var width = height * Screen.width / Screen.height;
                background.transform.localScale = new Vector3(height, width, 0);
            }

            if (tilemap != null)
            {
                SetTileMap(tilemap);
            }

            if (target == null)
            {
                target = GamePlay.Player.Instance.transform;
            }
        }

        private void LateUpdate()
        {
            if (target == null) return;
            var targetPosition = target.position;
            var newPosition = new Vector3(Mathf.Clamp(targetPosition.x, _xMin, _xMax),
                Mathf.Clamp(targetPosition.y, _yMin - Manager.GameManager.Instance.Ratio, _yMax), -10);
            transform.position = Vector3.Lerp(transform.position, newPosition, speed);
        }

        public void SetTileMap(Tilemap tm)
        {
            tm.CompressBounds();
            var minTile = tm.CellToWorld(tm.cellBounds.min);
            var maxTile = tm.CellToWorld(tm.cellBounds.max);
            SetLimits(minTile, maxTile);
        }

        private void SetLimits(Vector3 minTile, Vector3 maxTile)
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, nameof(mainCamera) + " != null");

            var height = 2f * mainCamera.orthographicSize;
            var width = height * mainCamera.aspect;

            _xMin = minTile.x + width / 2;
            _xMax = maxTile.x - width / 2;

            _yMin = minTile.y + height / 2;
            _yMax = maxTile.y - height / 2;
        }
    }
}