﻿using System;
using Random = System.Random;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class MathfHelper
    {
        private const double MIN_NORMAL = 2.2250738585072014E-308d;

        public static bool NearlyEqual(this float a, float b, float epsilon = 1f)
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (a.Equals(b)) return true;
            if (a.Equals(0) || b.Equals(0) || absA + absB < MIN_NORMAL)
                return diff < epsilon * MIN_NORMAL;
            return diff / (absA + absB) < epsilon;
        }

        public static float NextFloat(this Random rnd, float max, float min)
        {
            if (min > max) return (float) rnd.NextDouble() * (min - max) + max;
            return (float) rnd.NextDouble() * (max - min) + min;
        }

        public static double NextDouble(this Random rnd, double max, double min)
        {
            if (min > max) return rnd.NextDouble() * (min - max) + max;
            return rnd.NextDouble() * (max - min) + min;
        }

        public static string Minimum10(this float value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }
        
        public static string Minimum10(this int value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }
        
        public static string Minimum10(this double value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }
    }
}