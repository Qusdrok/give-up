﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class RigidbodyHelper
    {
        public static void SetAffectGravity(this Rigidbody2D rb, Collider2D collider, bool active)
        {
            rb.isKinematic = active;
            collider.isTrigger = active;
        }

        public static void Knockback(this Rigidbody2D rb, Vector2 direction, float thrustForce)
        {
            rb.AddForce(-direction * thrustForce);
        }

        public static void ResetVelocity(this Rigidbody2D rb)
        {
            rb.velocity = Vector2.zero;
        }

        public static void MoveWithVelocity(this Rigidbody2D rb, Vector2 direction, float moveSpeed)
        {
            rb.velocity = direction * (Time.fixedDeltaTime * moveSpeed);
        }

        public static void MoveToTarget(this Rigidbody2D rb, Transform origin, Transform target, float moveSpeed)
        {
            var position = origin.position;
            rb.MovePosition(position + (target.position - position) * moveSpeed * Time.fixedDeltaTime);
        }
    }
}