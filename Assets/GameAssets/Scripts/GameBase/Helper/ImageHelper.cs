﻿using System;
using System.Threading.Tasks;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class ImageHelper
    {
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");
        private static readonly int Progress = Shader.PropertyToID("_Progress");

        public static async void SceneTransition(this Image img, Sprite sprTransition, float transitionSpeed = 1f,
            Action onTransitionStarted = null, Action onTransitionCompleted = null)
        {
            onTransitionStarted?.Invoke();
            img.material.SetTexture(MainTex, sprTransition.texture);
            
            while (img.material.GetFloat(Progress) < 1.1f)
            {
                img.material.SetFloat(Progress, Mathf.MoveTowards(img.material.GetFloat(Progress),
                    1.1f, transitionSpeed * Time.deltaTime));
                await Task.Yield();
            }

            onTransitionCompleted?.Invoke();
        }
        
        public static async void FadeOut(this Image img, float fadeOutTime = 4f, Action onFadeOutStarted = null,
            Action onFadeOutCompleted = null)
        {
            var rate = 1f / fadeOutTime;
            var progress = 0f;
            
            img.color = Color.white;
            onFadeOutStarted?.Invoke();
            
            while (progress < 1f)
            {
                img.color = Color.Lerp(Color.white, Color.clear, progress);
                progress += rate * Time.deltaTime;
                await Task.Yield();
            }

            img.color = Color.clear;
            onFadeOutCompleted?.Invoke();
        }
        
        public static async void FadeIn(this Image img, float fadeInTime = 4f, Action onFadeInStarted = null,
            Action onFadeInCompleted = null)
        {
            var rate = 1f / fadeInTime;
            var progress = 0f;
            
            img.color = Color.clear;
            onFadeInStarted?.Invoke();
            
            while (progress < 1f)
            {
                img.color = Color.Lerp(Color.clear, Color.white, progress);
                progress += rate * Time.deltaTime;
                await Task.Yield();
            }

            img.color = Color.white;
            onFadeInCompleted?.Invoke();
        }
    }
}