﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    [RequireComponent(typeof(PoolingHelper))]
    public class SpawnerHelper : MonoBehaviour
    {
        private static PoolingHelper _pooling;

        public static GameObject CreateSpawner(Vector3 position, Transform parent, int index = 0,
            GameObject prefab = null, bool isSetLocal = false)
        {
            if (_pooling == null) _pooling = FindObjectOfType<PoolingHelper>();
            var clone = _pooling.GetObject(prefab == null ? _pooling.prefabs[index] : prefab, parent);

            if (isSetLocal) clone.transform.localPosition = position;
            else clone.transform.position = position;
            return clone;
        }

        public static void DestroySpawner(GameObject go, float delay = 0f)
        {
            _pooling.ReturnObject(go, delay);
        }

        public static GameObject CreateAndDestroy(Vector3 position, Transform parent, int index = 0,
            GameObject prefab = null, bool isSetLocal = false, float delay = 0f)
        {
            var clone = CreateSpawner(position, parent, index, prefab, isSetLocal);
            DestroySpawner(clone, delay);
            return clone;
        }
    }
}