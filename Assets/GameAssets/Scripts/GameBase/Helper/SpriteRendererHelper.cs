﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class SpriteRendererHelper
    {
        public static async void FadeOut(this SpriteRenderer sr, float fadeOutTime = 4f, Action onFadeOutStarted = null,
            Action onFadeOutCompleted = null)
        {
            var rate = 1f / fadeOutTime;
            var progress = 0f;

            sr.color = Color.white;
            onFadeOutStarted?.Invoke();

            while (progress < 1f)
            {
                sr.color = Color.Lerp(Color.white, Color.clear, progress);
                progress += rate * Time.deltaTime;
                await Task.Yield();
            }

            sr.color = Color.clear;
            onFadeOutCompleted?.Invoke();
        }

        public static async void FadeIn(this SpriteRenderer sr, float fadeInTime = 4f, Action onFadeInStarted = null,
            Action onFadeInCompleted = null)
        {
            var rate = 1f / fadeInTime;
            var progress = 0f;

            sr.color = Color.clear;
            onFadeInStarted?.Invoke();

            while (progress < 1f)
            {
                sr.color = Color.Lerp(Color.clear, Color.white, progress);
                progress += rate * Time.deltaTime;
                await Task.Yield();
            }

            sr.color = Color.white;
            onFadeInCompleted?.Invoke();
        }
    }
}