﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Helper
{
    public class SpawnerInColliderHelper : MonoBehaviour
    {
        [SerializeField] private List<GameObject> prefabs;

        [SerializeField] private int amount;

        private void Start()
        {
            var component = GetComponent<Collider>();
            var length = prefabs.Count - 1;
            var index = 0;
            
            while (index < amount)
            {
                var rdBound = RandomPos(component.bounds);
                var rdPos3D = new Vector3(rdBound.x, rdBound.y, rdBound.z);
                var rdPos = component.ClosestPoint(rdPos3D);

                if (!rdPos.x.NearlyEqual(rdPos3D.x) || !rdPos.y.NearlyEqual(rdPos3D.y)) continue;
                var rd = Random.Range(0, length);
                var clone = SpawnerHelper.CreateSpawner(Vector3.zero, transform, prefab: prefabs[rd]);

                clone.transform.position = new Vector3(rdPos3D.x, .5f, rdPos3D.z);
                index++;
            }
        }

        private static Vector3 RandomPos(Bounds bounds, float scale = 1f)
        {
            var min = bounds.min;
            var max = bounds.max;
            return new Vector3(Random.Range(min.x * scale, max.x * scale),
                Random.Range(min.y * scale, max.y * scale),
                Random.Range(min.z * scale, max.z * scale));
        }
    }
}