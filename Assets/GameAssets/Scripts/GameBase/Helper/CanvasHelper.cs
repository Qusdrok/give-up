﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class CanvasHelper
    {
        public static void OpenClose(this CanvasGroup cg)
        {
            cg.alpha = cg.alpha > 0 ? 0 : 1;
            cg.blocksRaycasts = cg.blocksRaycasts != true;
        }
    }
}