﻿using System;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Haptic
{
    public enum HapticType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact
    }

    public static class AndroidHaptic
    {
        private const long LightDuration = 20;
        private const long MediumDuration = 40;
        private const long HeavyDuration = 80;
        private const int LightAmplitude = 40;
        private const int MediumAmplitude = 120;
        private const int HeavyAmplitude = 255;
        private static int _sdkVersion = -1;

        private static readonly long[] SuccessPattern =
        {
            0, LightDuration,
            LightDuration,
            HeavyDuration
        };

        private static readonly int[] SuccessPatternAmplitude =
        {
            0, LightAmplitude,
            0, HeavyAmplitude
        };

        private static readonly long[] WarningPattern =
        {
            0, HeavyDuration,
            LightDuration, MediumDuration
        };

        private static readonly int[] WarningPatternAmplitude =
        {
            0, HeavyAmplitude,
            0, MediumAmplitude
        };

        private static readonly long[] FailurePattern =
        {
            0, MediumDuration,
            LightDuration, MediumDuration,
            LightDuration, HeavyDuration,
            LightDuration, LightDuration
        };

        private static readonly int[] FailurePatternAmplitude =
        {
            0, MediumAmplitude,
            0, MediumAmplitude,
            0, HeavyAmplitude,
            0, LightAmplitude
        };

        private static void VibrateHandheld()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    Handheld.Vibrate();
                    break;
            }
        }

        public static void Vibrate()
        {
            AndroidVibrate(MediumDuration);
        }

        public static void Haptic(HapticType type)
        {
            switch (type)
            {
                case HapticType.Selection:
                    AndroidVibrate(LightDuration, LightAmplitude);
                    break;

                case HapticType.Success:
                    AndroidVibrate(SuccessPattern, SuccessPatternAmplitude, -1);
                    break;

                case HapticType.Warning:
                    AndroidVibrate(WarningPattern, WarningPatternAmplitude, -1);
                    break;

                case HapticType.Failure:
                    AndroidVibrate(FailurePattern, FailurePatternAmplitude, -1);
                    break;

                case HapticType.LightImpact:
                    AndroidVibrate(LightDuration, LightAmplitude);
                    break;

                case HapticType.MediumImpact:
                    AndroidVibrate(MediumDuration, MediumAmplitude);
                    break;

                case HapticType.HeavyImpact:
                    AndroidVibrate(HeavyDuration, HeavyAmplitude);
                    break;
            }
        }

#if UNITY_IOS && !UNITY_EDITOR
        private static AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        private static AndroidJavaObject CurrentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        private static AndroidJavaObject AndroidVibrator =
            CurrentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
        private static AndroidJavaClass VibrationEffectClass;
        private static AndroidJavaObject VibrationEffect;
        private static int DefaultAmplitude;
#else
        private static AndroidJavaClass _unityPlayer;
        private static AndroidJavaObject _currentActivity;
        private static readonly AndroidJavaObject AndroidVibrator = null;
        private static AndroidJavaClass _vibrationEffectClass;
        private static AndroidJavaObject _vibrationEffect;
        private static int _defaultAmplitude;
#endif

        public static void AndroidVibrate(long milliseconds)
        {
            AndroidVibrator.Call("vibrate", milliseconds);
        }

        public static void AndroidVibrate(long milliseconds, int amplitude)
        {
            if (AndroidSDKVersion() < 26)
            {
                AndroidVibrate(milliseconds);
            }
            else
            {
                VibrationEffectClassInitialization();
                _vibrationEffect =
                    _vibrationEffectClass.CallStatic<AndroidJavaObject>("createOneShot", milliseconds, amplitude);
                AndroidVibrator.Call("vibrate", _vibrationEffect);
            }
        }

        public static void AndroidVibrate(long[] pattern, int repeat)
        {
            if (AndroidSDKVersion() < 26)
            {
                AndroidVibrator.Call("vibrate", pattern, repeat);
            }
            else
            {
                VibrationEffectClassInitialization();
                _vibrationEffect =
                    _vibrationEffectClass.CallStatic<AndroidJavaObject>("createWaveform", pattern, repeat);
                AndroidVibrator.Call("vibrate", _vibrationEffect);
            }
        }

        public static void AndroidVibrate(long[] pattern, int[] amplitudes, int repeat)
        {
            if (AndroidSDKVersion() < 26)
            {
                AndroidVibrator.Call("vibrate", pattern, repeat);
            }
            else
            {
                VibrationEffectClassInitialization();
                _vibrationEffect =
                    _vibrationEffectClass.CallStatic<AndroidJavaObject>("createWaveform", pattern, amplitudes, repeat);
                AndroidVibrator.Call("vibrate", _vibrationEffect);
            }
        }

        public static void AndroidCancelVibrations()
        {
            AndroidVibrator.Call("cancel");
        }

        private static void VibrationEffectClassInitialization()
        {
            if (_vibrationEffectClass != null) return;
            _vibrationEffectClass = new AndroidJavaClass("android.os.VibrationEffect");
        }

        public static int AndroidSDKVersion()
        {
            if (_sdkVersion != -1) return _sdkVersion;
            var apiLevel =
                int.Parse(SystemInfo.operatingSystem.Substring(
                    SystemInfo.operatingSystem.IndexOf("-", StringComparison.Ordinal) + 1, 3));

            _sdkVersion = apiLevel;
            return apiLevel;
        }
    }
}