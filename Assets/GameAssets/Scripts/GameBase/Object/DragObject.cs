﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GameBase.Object
{
    public class DragObject : MonoBehaviour, IDragHandler
    {
        private RectTransform _rectTransform;
        private Canvas _canvas;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvas = transform.parent.GetComponent<Canvas>();
        }

        public void OnDrag(PointerEventData ped)
        {
            _rectTransform.anchoredPosition += ped.delta / _canvas.scaleFactor;
        }
    }
}