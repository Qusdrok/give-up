﻿using GameAssets.Scripts.GameBase.Base;

namespace GameAssets.Scripts.GameBase.Interface
{
    public interface IState
    {
        BaseCharacter Character { get; set; }
        void StartState();
        void UpdateState();
        void ExitState();
    }
}