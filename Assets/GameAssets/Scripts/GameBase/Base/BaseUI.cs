﻿using DG.Tweening;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseUI : MonoBehaviour
    {
        [SerializeField] private AnimationCurve animationLine;
        [SerializeField] private float durationOn = 0.2f;
        [SerializeField] private float durationOff = 0.02f;

        private RectTransform _rectTransform;

        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
        }

        public void AnimationZoom(bool active)
        {
            if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>();
            if (active)
            {
                _rectTransform.DOScale(Vector3.one, durationOn).SetEase(animationLine)
                    .OnComplete(() => gameObject.SetActive(true));
            }
            else
            {
                _rectTransform.DOScale(Vector3.zero, durationOff).OnComplete(() => gameObject.SetActive(false));
            }
        }
    }
}