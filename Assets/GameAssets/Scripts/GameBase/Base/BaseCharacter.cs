﻿using System;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Interface;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Manager.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Base
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BaseCharacter : MonoBehaviour
    {
        [SerializeField] protected float moveSpeed;
        [SerializeField] private LayerMask groundLayer;
        [ReadOnly] public CharacterState characterState;

        public IState CurrentState;

        protected Rigidbody2D Rigidbody;
        protected Animator Animator;
        protected CapsuleCollider2D CapsuleCollider;
        protected SpriteRenderer SpriteRenderer;

        protected UIPlay UIPlay;
        protected UIGameStart UIGameStart;

        private static readonly int State = Animator.StringToHash("State");
        private int _currentIndex;

        private bool IsMove { get; set; }
        protected bool IsAttack { get; set; }
        public bool IsAlive { get; private set; } = true;

        #region Virtual Monobehaviour

        protected virtual void Awake()
        {
            CapsuleCollider = GetComponent<CapsuleCollider2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            Animator = GetComponent<Animator>();
            Rigidbody = GetComponent<Rigidbody2D>();

            UIPlay = UIManager.Instance.GetUI<UIPlay>();
            UIGameStart = UIManager.Instance.GetUI<UIGameStart>();
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void Start()
        {
        }

        private void Update()
        {
            if (!Manager.GameManager.Instance.IsGameState(GameState.Playing) || !IsAlive) return;
            InnerUpdate();
        }

        private void FixedUpdate()
        {
            if (!Manager.GameManager.Instance.IsGameState(GameState.Playing) || !IsAlive) return;
            InnerFixedUpdate();
        }

        private void LateUpdate()
        {
            if (!Manager.GameManager.Instance.IsGameState(GameState.Playing) || !IsAlive) return;
            InnerLateUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void InnerFixedUpdate()
        {
        }

        protected virtual void InnerLateUpdate()
        {
        }

        protected virtual void OnCollisionEnter2D(Collision2D other)
        {
        }

        protected virtual void OnCollisionStay2D(Collision2D other)
        {
        }

        protected virtual void OnCollisionExit2D(Collision2D other)
        {
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
        }

        protected virtual void OnParticleCollision(GameObject other)
        {
        }

        protected virtual void OnCollisionEnter(Collision other)
        {
        }

        protected virtual void OnCollisionStay(Collision other)
        {
        }

        protected virtual void OnCollisionExit(Collision other)
        {
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
        }

        protected virtual void OnTriggerStay(Collider other)
        {
        }

        protected virtual void OnTriggerExit(Collider other)
        {
        }

        protected virtual void OnDestroy()
        {
        }

        #endregion

        public bool IsGround()
        {
            var bounds = CapsuleCollider.bounds;
            var hit = Physics2D.Raycast(bounds.center, Vector2.down, bounds.extents.y + .01f, groundLayer);
            return hit.collider != null && hit.collider.CompareTag("Ground");
        }

        public bool IsCharacterState(CharacterState state) => characterState.Equals(state);

        public void ChangeAnimationState(int index)
        {
            if (_currentIndex.Equals(index)) return;
            _currentIndex = index;
            Animator.SetInteger(State, index);
        }

        public void ChangeAnimatorLayer(string layerName, bool active = true)
        {
            for (var i = 0; i < Animator.layerCount; i++)
            {
                Animator.SetLayerWeight(i, 0);
            }

            Animator.SetLayerWeight(Animator.GetLayerIndex(layerName), active ? 1 : 0);
        }

        public void ChangeState(IState newState = null)
        {
            if ((CurrentState?.ToString() ?? "Null").Equals(newState?.ToString() ?? "Null"))
            {
                Rigidbody.ResetVelocity();
                return;
            }

            CurrentState?.ExitState();
            CurrentState = newState;

            if (CurrentState == null)
            {
                Rigidbody.ResetVelocity();
                return;
            }

            Debug.Log($"CURRENT STATE ===> {newState}");
            CurrentState.Character = this;
            CurrentState.StartState();
        }

        public bool ChangeCharacterState(CharacterState newState)
        {
            if (!IsAlive) return false;
            characterState = newState;

            switch (newState)
            {
                case CharacterState.Idle:
                    IsMove = false;
                    IsAlive = true;
                    IsAttack = false;
                    break;
                case CharacterState.Move:
                    IsMove = true;
                    IsAttack = false;
                    IsAlive = true;
                    break;
                case CharacterState.Attack:
                    IsMove = false;
                    IsAttack = true;
                    IsAlive = true;
                    break;
                case CharacterState.Death:
                    IsMove = false;
                    IsAttack = false;
                    IsAlive = false;
                    break;
                case CharacterState.NonAttack:
                    IsAttack = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
            }

            return true;
        }
    }
}