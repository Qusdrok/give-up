﻿using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Manager.UI;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<T>();
                return _instance;
            }
            private set => _instance = value;
        }

        [SerializeField] private bool isDontDestroy;

        protected virtual void Awake()
        {
            if (Instance == null || !isDontDestroy) return;
            if (transform.parent != null) transform.SetParent(null);
            DontDestroyOnLoad(this);
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void Start()
        {
        }

        private void Update()
        {
            if (!Manager.GameManager.Instance.IsGameState(GameState.Playing)) return;
            InnerUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void FixedUpdate()
        {
        }

        protected virtual void LateUpdate()
        {
        }

        protected virtual void OnDestroy()
        {
            if (Equals(Instance, this)) Instance = default;
        }
    }
}