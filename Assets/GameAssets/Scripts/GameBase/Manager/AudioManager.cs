﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Haptic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    [Serializable]
    public class AudioConfig
    {
        [TableColumnWidth(30)] public string clipName;
        [TableColumnWidth(30)] public AudioClip clipSound;
    }

    public class AudioManager : BaseSingleton<AudioManager>
    {
        [TableList] [SerializeField] private List<AudioConfig> audioClips = new List<AudioConfig>();
        [SerializeField] private AudioSource audioSound;
        [SerializeField] private AudioSource audioMusic;

        private int _loop;

        protected override void Awake()
        {
            base.Awake();
            OnEnableAudio();
        }

        private void OnEnableAudio()
        {
            var volumeSound = UserDataManager.Instance.userDataSave.sound ? 1 : 0;
            var volumeMusic = UserDataManager.Instance.userDataSave.music ? 1 : 0;

            audioSound.volume = volumeSound;
            audioMusic.volume = volumeMusic;

            if (volumeMusic.Equals(1))
            {
                PlayMusic("Background");
            }
            else
            {
                StopMusic();
            }
        }

        public void PlayMusic(string clipName)
        {
            var clip = GetClip(clipName);
            if (clip == null) return;

            audioMusic.volume = 1;
            audioMusic.clip = clip;
            audioMusic.Play();
        }

        public void StopMusic()
        {
            UserDataManager.Instance.SetMusic(false);
            audioMusic.Stop();
        }

        public void StopAll()
        {
            StopMusic();
            StopSound();
        }

        public void PlaySoundLoop(string clipName, int loopTimes = 1)
        {
            var clip = GetClip(clipName);
            if (clip == null) return;
            audioSound.volume = 1;
            
            if (loopTimes < 2)
            {
                audioSound.PlayOneShot(clip);
            }
            else
            {
                _loop = 0;
                audioSound.clip = clip;
                StartCoroutine(Loop(clip, loopTimes));
            }
        }

        public void StopSound()
        {
            UserDataManager.Instance.SetSound(false);
            audioSound.Stop();
        }

        public void PlayVibrate()
        {
            if (UserDataManager.Instance.userDataSave.vibrate)
            {
                AndroidHaptic.Vibrate();
            }
        }

        private AudioClip GetClip(string soundName)
        {
            return (from x in audioClips where x.clipName.Equals(soundName) select x.clipSound).FirstOrDefault();
        }

        private IEnumerator Loop(AudioClip clip, int loop)
        {
            audioSound.Play();
            _loop += 1;

            yield return new WaitForSeconds(clip.length / 2);
            if (_loop < loop) StartCoroutine(Loop(clip, loop));
        }
    }
}