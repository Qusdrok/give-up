﻿using UnityEngine;
using System;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Manager.UI;
using UnityEngine.Purchasing;

public class IAPManager : BaseSingleton<IAPManager>, IStoreListener
{
    private static IStoreController _storeController;
    private static IExtensionProvider _storeExtensionProvider;

    private const string PRODUCT_REMOVE_ADS = "remove_ads";

    protected override void Start()
    {
        if (_storeController == null) InitializePurchasing();
    }

    private void InitializePurchasing()
    {
        if (IsInitialized()) return;
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        
        builder.AddProduct(PRODUCT_REMOVE_ADS, ProductType.Consumable);
        UnityPurchasing.Initialize(this, builder);
    }

    private static bool IsInitialized()
    {
        return _storeController != null && _storeExtensionProvider != null;
    }

    public void BuyRemoveAds()
    {
        BuyProductID(PRODUCT_REMOVE_ADS);
    }

    private static void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            var product = _storeController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log($"Purchasing product async: '{product.definition.id}'");
                _storeController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log(
                    "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        _storeController = controller;
        _storeExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (string.Equals(args.purchasedProduct.definition.id, PRODUCT_REMOVE_ADS, StringComparison.Ordinal))
        {
            UIManager.Instance.GetUI<UIGameStart>().RemoveAds();
        }
        else
        {
            Debug.Log($"ProcessPurchase: FAIL. Unrecognized product: '{args.purchasedProduct.definition.id}'");
        }

        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(
            $"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");
    }
}