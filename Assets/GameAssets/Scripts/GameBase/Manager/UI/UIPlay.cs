﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Helper;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIPlay : BaseUI
    {
        [Space(15)] [SerializeField] private Animator loadingBarAnimator;
        [SerializeField] private TextMeshProUGUI txtInformation;
        [SerializeField] private TextMeshProUGUI txtQuit;

        [SerializeField] private Image imgSceneTransition;
        [SerializeField] private Image imgMusic;

        [SerializeField] private Button btnPlay;
        [SerializeField] private Button btnMusic;
        [SerializeField] private Button btnGiveUp;
        [SerializeField] private Button btnJump;

        [SerializeField] private Button btnPause;
        [SerializeField] private Button btnPauseContinue;
        [SerializeField] private Button btnPauseExitToMenu;

        [SerializeField] private Button btnQuitYes;
        [SerializeField] private Button btnQuitNo;

        [SerializeField] private CanvasGroup cgPauseDialogue;
        [SerializeField] private CanvasGroup cgQuitDialogue;

        protected override void Start()
        {
            base.Start();
            if (loadingBarAnimator != null)
            {
                loadingBarAnimator.OnCompleted(() =>
                {
                    btnPlay.gameObject.SetActive(true);
                    btnPlay.onClick.AddListener(() => UIManager.Instance.HideAllAndShowUI(UIType.UIGameStart));
                });
            }

            if (btnMusic != null)
            {
                AssetManager.Instance.SetSpriteMusic(imgMusic);
                btnMusic.onClick.AddListener(() =>
                {
                    if (UserDataManager.Instance.userDataSave.music)
                    {
                        AudioManager.Instance.StopMusic();
                    }
                    else
                    {
                        UserDataManager.Instance.SetMusic(true);
                        AudioManager.Instance.PlayMusic("Background");
                    }
                    
                    AssetManager.Instance.SetSpriteMusic(imgMusic);
                });
            }

            if (btnGiveUp != null)
            {
                btnGiveUp.onClick.AddListener(() =>
                {
                    if (!GameManager.Instance.IsGameState(GameState.Playing)) return;
                    txtQuit.text = "Do you want to really give up ?";
                    cgQuitDialogue.OpenClose();
                });
            }

            if (btnJump != null)
            {
                btnJump.onClick.AddListener(() =>
                {
                    if (!GameManager.Instance.IsGameState(GameState.Playing)) return;
                    GamePlay.Player.Instance.Jump();
                });
            }

            if (btnPause != null)
            {
                btnPause.onClick.AddListener(() =>
                {
                    if (!GameManager.Instance.IsGameState(GameState.Playing)) return;
                    cgPauseDialogue.OpenClose();
                    GameManager.Instance.SetGameState(GameState.Pause);
                });

                btnPauseContinue.onClick.AddListener(() =>
                {
                    cgPauseDialogue.OpenClose();
                    GameManager.Instance.SetGameState(GameState.Playing);
                });

                btnPauseExitToMenu.onClick.AddListener(() =>
                {
                    txtQuit.text = "Do you want to really quit ?";
                    cgQuitDialogue.OpenClose();
                    cgPauseDialogue.OpenClose();
                });
            }

            if (btnQuitYes != null)
            {
                btnQuitYes.onClick.AddListener(() => SceneManager.LoadSceneAsync("GameMenu"));
                btnQuitNo.onClick.AddListener(() =>
                {
                    cgQuitDialogue.OpenClose();
                    GameManager.Instance.SetGameState(GameState.Playing);
                });
            }

            UpdateInformation();
        }

        public void UpdateInformation()
        {
            var data = UserDataManager.Instance.userDataSave;
            var percent = data.level / 40;
            txtInformation.text =
                $"F{data.level.Minimum10()}, {percent.Minimum10()}% Done | {data.death.Minimum10()} Deaths | {data.GetTimePlayed()}";
        }

        public void SceneTransition()
        {
            imgSceneTransition.SceneTransition(AssetManager.Instance.RandomTransition(),
                onTransitionStarted: () => imgSceneTransition.gameObject.SetActive(true));
        }
    }
}