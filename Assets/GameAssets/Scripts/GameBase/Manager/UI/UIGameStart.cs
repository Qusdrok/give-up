﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameStart : BaseUI
    {
        [Space(15)] [SerializeField] private List<Button> btnBonusLevel = new List<Button>();
        [SerializeField] private Button btnPlay;
        [SerializeField] private Button btnScores;
        [SerializeField] private Button btnRemoveAds;

        protected override void Start()
        {
            base.Start();
            if (btnPlay != null)
            {
                btnPlay.onClick.AddListener(() => { SceneManager.LoadSceneAsync("GamePlay"); });
            }

            if (btnScores != null)
            {
                btnScores.onClick.AddListener(() => { UIManager.Instance.HideAllAndShowUI(UIType.UIStore); });
            }

            if (btnRemoveAds != null)
            {
                btnRemoveAds.onClick.AddListener(() =>
                {
                    IAPManager.Instance.BuyRemoveAds();
                });
            }
        }

        public void RemoveAds()
        {
            if (btnRemoveAds == null) return;
            btnRemoveAds.gameObject.SetActive(false);

            UserDataManager.Instance.SetRemovedAds();
            Debug.Log("REMOVED ADS SUCCESS");
        }
    }
}