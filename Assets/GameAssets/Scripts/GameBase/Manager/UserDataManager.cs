﻿using System;
using System.Collections.Generic;
using System.Globalization;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    [Serializable]
    public class Highscore
    {
        public string name;
        public string timer;
        public string deaths;
        public string floor;

        public Highscore(string name, string timer, string deaths, string floor)
        {
            this.name = name;
            this.timer = timer;
            this.deaths = deaths;
            this.floor = floor;
        }
    }

    [Serializable]
    public class UserDataSave
    {
        public int level;
        [ReadOnly] public int bestLevel;
        [ReadOnly] public int bestScore;
        [ReadOnly] public int bestScoreEndless;

        public int diamond;
        public int money;
        public int death;

        [ReadOnly] public float hours;
        [ReadOnly] public float minutes;
        [ReadOnly] public float seconds;

        public bool sound;
        public bool music;
        public bool vibrate;
        public bool removedAds;

        [ReadOnly] public string lastOpenDay;
        [ReadOnly] public string versionCode;

        public List<Highscore> highscores = new List<Highscore>();

        public string GetTimePlayed()
        {
            return $"{hours.Minimum10()}:{minutes.Minimum10()}:{seconds.Minimum10()}";
        }
    }

    public class UserDataManager : BaseSingleton<UserDataManager>
    {
        [ReadOnly] [SerializeField] private int currentLevel;

        private const string SAVE_DATA = "UserSaveData";
        private const int INTERVAL = 10;

        public UserDataSave userDataSave;
        private UIPlay _uiPlay;

        protected override void Awake()
        {
            base.Awake();
            if (GetDataFromSave() == null) ResetUserData();
            userDataSave = GetDataFromSave();
        }

        protected override void Start()
        {
            base.Start();
            _uiPlay = UIManager.Instance.GetUI<UIPlay>();
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            if (Time.frameCount % INTERVAL != 0) return;
            if (userDataSave.seconds >= 59)
            {
                userDataSave.minutes++;
                userDataSave.seconds = 0;
            }

            if (userDataSave.minutes >= 59)
            {
                userDataSave.hours++;
                userDataSave.minutes = 0;
            }

            userDataSave.seconds++;
            _uiPlay.UpdateInformation();
        }

        private static UserDataSave GetDataFromSave()
        {
            var json = PlayerPrefs.GetString(SAVE_DATA);
            if (json.Length > 0) Debug.Log($"GAME DATA ===> {json}");
            return JsonUtility.FromJson<UserDataSave>(json);
        }

        private void Save()
        {
            currentLevel = userDataSave.level;
            var json = JsonUtility.ToJson(userDataSave);

            Debug.Log($"GAME DATA SAVED ===> {json}");
            PlayerPrefs.SetString(SAVE_DATA, json);
            PlayerPrefs.Save();
        }

        [Button("Clear User Data")]
        private void ClearUserData()
        {
            PlayerPrefs.DeleteAll();
        }

        [Button("Reset User Data")]
        private void ResetUserData()
        {
            userDataSave = new UserDataSave
            {
                level = 1,
                bestScore = 0,
                bestScoreEndless = 0,
                sound = true,
                music = true,
                vibrate = true,
                diamond = 0,
                money = 0,
                lastOpenDay = DateTime.Now.ToString(CultureInfo.InvariantCulture)
            };

            Save();
        }

        public void SetMusic(bool enable)
        {
            userDataSave.music = enable;
            Save();
        }

        public void SetSound(bool enable)
        {
            userDataSave.sound = enable;
            Save();
        }

        public void SetHaptic(bool enable)
        {
            userDataSave.vibrate = enable;
            Save();
        }

        public void SetBestScoreLevel(int score)
        {
            if (score > userDataSave.bestScore) userDataSave.bestScore = score;
            Save();
        }

        public void SetBestScoreEndless(int score)
        {
            if (score > userDataSave.bestScoreEndless) userDataSave.bestScoreEndless = score;
            Save();
        }

        public void LevelUp()
        {
            userDataSave.level++;
            Save();
        }

        public void AddDeath()
        {
            userDataSave.death++;
            Save();
        }

        public void AddMoney(int value)
        {
            userDataSave.money += value;
            Save();
        }

        public void AddDiamond(int value)
        {
            userDataSave.diamond += value;
            Save();
        }

        public void AddHighscore(Highscore hs)
        {
            userDataSave.highscores.Add(hs);
            Save();
        }

        public void SetBestLevel(int level)
        {
            if (level > userDataSave.bestLevel) userDataSave.bestLevel = level;
            Save();
        }

        public void SetRemovedAds()
        {
            userDataSave.removedAds = true;
            Save();
        }
    }
}