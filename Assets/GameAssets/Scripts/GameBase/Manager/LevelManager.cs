﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class LevelManager : BaseSingleton<LevelManager>
    {
        [SerializeField] private List<GameObject> levels = new List<GameObject>();
        [SerializeField] private Transform parent;
        [SerializeField] private CameraFollowHelper mainCamera;
        [ReadOnly] public int currentLevel;

        protected override void Awake()
        {
            base.Awake();
            currentLevel = UserDataManager.Instance.userDataSave.level;
            LoadLevel();
        }

        private void LoadLevel()
        {
            if (levels.Count <= 0) return;
            if (currentLevel > levels.Count - 1) currentLevel = levels.Count - 1;

            var clone = SpawnerHelper.CreateSpawner(new Vector3(0, GameManager.Instance.Ratio, 0), parent,
                prefab: levels[currentLevel]);
            mainCamera.SetTileMap(clone.GetComponentInChildren<Tilemap>());
        }
    }
}