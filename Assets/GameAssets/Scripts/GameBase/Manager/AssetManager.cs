﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class AssetManager : BaseSingleton<AssetManager>
    {
        [Space] [Header("Sprite")] [SerializeField]
        private List<Sprite> uiTransition = new List<Sprite>();

        public Sprite uiSoundOn;
        public Sprite uiSoundOff;
        public Sprite uiMusicOn;
        public Sprite uiMusicOff;
        public Sprite uiHapticOn;
        public Sprite uiHapticOff;
        public Sprite uiUseWatchAds;
        public Sprite uiUseByMoney;
        public Sprite uiUseByDiamond;
        public Sprite uiBtnBuyColor;
        public Sprite uiBtnBought;

        private readonly Random _rnd = new Random();

        public Sprite RandomTransition()
        {
            return uiTransition[_rnd.Next(0, uiTransition.Count - 1)];
        }

        public void SetSpriteMusic(Image imgMusic)
        {
            imgMusic.sprite = UserDataManager.Instance.userDataSave.music ? uiMusicOn : uiMusicOff;
        }

        public void SetSpriteSound(Image imgSound)
        {
            imgSound.sprite = UserDataManager.Instance.userDataSave.sound ? uiSoundOn : uiSoundOff;
        }

        public void SetSpriteHaptic(Image imgHaptic)
        {
            imgHaptic.sprite = UserDataManager.Instance.userDataSave.vibrate ? uiHapticOn : uiHapticOff;
        }
    }
}