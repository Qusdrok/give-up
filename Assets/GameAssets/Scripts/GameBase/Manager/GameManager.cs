﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.GamePlay;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class GameManager : BaseSingleton<GameManager>
    {
        [ReadOnly] [SerializeField] private GameState currentState;
        [ReadOnly] [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();
        
        public Camera Camera { get; private set; }
        public float Ratio { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;

            Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            Ratio = Screen.width / Screen.height + 0.15f;
            AddBaseCharacter(FindObjectsOfType<BaseCharacter>());
        }

        public void AddBaseCharacter(params BaseCharacter[] character)
        {
            characters.AddRange(character);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            foreach (var x in characters)
            {
                x.CurrentState?.UpdateState();
            }
        }

        public void SetGameState(GameState gameState)
        {
            if (currentState.Equals(gameState)) return;
            currentState = gameState;
            Debug.Log($"CURRENT GAME STATE ===> {gameState}");
        }

        public bool IsGameState(GameState state)
        {
            return currentState == state;
        }

        public void OnWin()
        {
            SetGameState(GameState.Win);
            UIManager.Instance.HideAllAndShowUI(UIType.UIGameComplete);
        }

        public void OnLose()
        {
            SetGameState(GameState.Lose);
            UIManager.Instance.HideAllAndShowUI(UIType.UIGameOver);
        }
    }
}