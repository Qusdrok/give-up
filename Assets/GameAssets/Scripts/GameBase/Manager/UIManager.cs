﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class UIManager : BaseSingleton<UIManager>
    {
        [SerializeField] private List<BaseUI> uis = new List<BaseUI>();
        [Space(10)] [SerializeField] private UIType uiStart = UIType.UIPlay;

        protected override void Start()
        {
            base.Start();
            HideAllAndShowUI(uiStart);
        }

        public T GetUI<T>()
        {
            foreach (var t in uis.Select(x => x.GetComponent<T>()).Where(t => t != null))
            {
                return t;
            }

            return default;
        }

        public void HideAllAndShowUI(UIType uiType)
        {
            foreach (var x in uis)
            {
                var active = x.name.Equals(uiType.ToString());
                if (!x.gameObject.activeInHierarchy) return;
                
                x.AnimationZoom(active);
                x.gameObject.SetActive(active);
            }
        }
    }
}