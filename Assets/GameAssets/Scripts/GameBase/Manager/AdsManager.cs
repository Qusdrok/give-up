﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class AdsManager : MonoBehaviour
    {
        private const string ADS_ID_ANDROID = "3696187";
        private const string VIDEO_ADS = "video";

        private void Start()
        {
            Debug.Log($"Platform is {(Advertisement.isSupported ? "" : "not ")}supported\n" +
                $"Unity Ads {(Advertisement.isInitialized ? "" : "not ")}initialized");
            if (!Advertisement.isInitialized) Advertisement.Initialize(ADS_ID_ANDROID, false);
        }

        public static void ShowAds()
        {
            if (UserDataManager.Instance.userDataSave.removedAds) return;
            if (Advertisement.IsReady(VIDEO_ADS))
            {
#pragma warning disable 618
                Advertisement.Show(VIDEO_ADS, new ShowOptions {resultCallback = HandleShowResult});
#pragma warning restore 618
            }
            else
            {
                Debug.Log("Ads not ready");
            }
        }

        private static void HandleShowResult(ShowResult result)
        {
            switch (result)
            {
                case ShowResult.Failed:
                    Debug.Log("ADS FAILED");
                    break;
                case ShowResult.Finished:
                    Debug.Log("ADS FINISHED");
                    break;
                case ShowResult.Skipped:
                    Debug.Log("ADS SKIPPED");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(result), result, null);
            }
        }
    }
}