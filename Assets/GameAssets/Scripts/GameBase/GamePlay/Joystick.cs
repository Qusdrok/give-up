﻿using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class Joystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Image bgJoystick;
        [SerializeField] private Image icJoystick;

        [SerializeField] private bool isHorizontal;

        public Vector2 Direction { get; private set; } = Vector2.zero;

        public void OnDrag(PointerEventData eventData)
        {
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(bgJoystick.rectTransform, eventData.position,
                eventData.pressEventCamera, out var pos)) return;
            var delta = bgJoystick.rectTransform.sizeDelta;
            var pivot = bgJoystick.rectTransform.pivot;

            pos.x /= delta.x;
            pos.y /= delta.y;

            var x = pivot.x.NearlyEqual(1) ? pos.x * 2 + 1 : pos.x * 2;
            var y = pivot.y.NearlyEqual(1) ? pos.y * 2 + 1 : pos.y * 2;

            Direction = new Vector2(x, isHorizontal ? 0 : y);
            Direction = Direction.magnitude >= 1f ? Direction.normalized : Direction;
            icJoystick.rectTransform.anchoredPosition =
                new Vector2(Direction.x * (delta.x / 3), Direction.y * (delta.y / 3));
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnDrag(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Player.Instance.SetIdle();
            Direction = Vector2.zero;
            icJoystick.rectTransform.anchoredPosition = Vector2.zero;
        }
    }
}