﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class Player : BaseCharacter
    {
        [SerializeField] private float thrustForce = 25f;
        [SerializeField] private float fadeoutTime = 4f;
        [SerializeField] private float fadeinTime = 4f;

        private Joystick _joystick;
        private Vector2 _direction;

        private bool _faceRight = true;
        private bool _isJumping;
        private bool _isDoubleJumping;

        #region Singleton

        public static Player Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null) Instance = this;
            _joystick = FindObjectOfType<Joystick>();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Equals(Instance, this)) Instance = null;
        }

        #endregion

        protected override void Start()
        {
            base.Start();
            SpriteRenderer.FadeIn(fadeinTime,
                onFadeInCompleted: () => Manager.GameManager.Instance.SetGameState(GameState.Playing));
        }

        protected override void InnerFixedUpdate()
        {
            base.InnerFixedUpdate();
            MoveDirection();
        }

        private void MoveDirection()
        {
            _direction = _joystick.Direction;
            if (_direction.Equals(Vector2.zero) || _isJumping || _isDoubleJumping) return;
            if (_direction.x < 0 && _faceRight || _direction.x > 0 && !_faceRight)
            {
                var trans = transform;
                var scale = trans.localScale;

                scale.x *= -1;
                trans.localScale = scale;
                _faceRight = !_faceRight;
            }

            ChangeAnimationState(1);
            Rigidbody.MoveWithVelocity(_direction, moveSpeed);
        }

        public void Jump()
        {
            if (!IsAlive) return;
            if (!_isJumping && !_isDoubleJumping)
            {
                _isJumping = true;
                _isDoubleJumping = true;
                
                Rigidbody.AddForce(Vector2.up * thrustForce, ForceMode2D.Force);
                ChangeAnimationState(2);
                Animator.OnCompleted(() => ChangeAnimationState(3));
            }
            else if (_isDoubleJumping)
            {
                _isDoubleJumping = false;
                Rigidbody.AddForce(Vector2.up * thrustForce, ForceMode2D.Force);
                ChangeAnimationState(2);
                Animator.OnCompleted(() => ChangeAnimationState(3));
            }
        }

        public void SetIdle()
        {
            Rigidbody.ResetVelocity();
            ChangeAnimationState(0);
        }

        public void Death()
        {
            ChangeCharacterState(CharacterState.Death);
            UserDataManager.Instance.AddDeath();
            SceneManager.LoadSceneAsync("GamePlay");
        }

        protected override void OnCollisionEnter2D(Collision2D other)
        {
            base.OnCollisionEnter2D(other);
            if (!other.collider.CompareTag("Ground")) return;

            _isJumping = false;
            _isDoubleJumping = false;
            ChangeAnimationState(0);
        }

        protected override void OnTriggerStay2D(Collider2D other)
        {
            base.OnTriggerStay2D(other);
            if (other.CompareTag("Finish"))
            {
                SpriteRenderer.FadeOut(fadeoutTime,
                    onFadeOutCompleted: () =>
                    {
                        Manager.GameManager.Instance.SetGameState(GameState.Win);
                        UIPlay.SceneTransition();
                    });
            }
        }

        protected override void OnCollisionExit2D(Collision2D other)
        {
            base.OnCollisionExit2D(other);
            if (other.collider.CompareTag("Ground"))
            {
                ChangeAnimationState(3);
            }
        }
    }
}