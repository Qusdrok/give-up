﻿using System;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float moveSpeed = 2f;

        private Rigidbody2D _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void Move(Vector2 direction)
        {
            _rigidbody.MoveWithVelocity(direction, moveSpeed);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.collider.tag)
            {
                case "Player":
                    SpawnerHelper.DestroySpawner(gameObject);
                    Player.Instance.Death();
                    break;
                case "Ground":
                    SpawnerHelper.DestroySpawner(gameObject);
                    break;
            }
        }
    }
}