﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour
{
    [SerializeField]
    private float speedX, speedY, maxPosY, minPosY, range;

    [SerializeField]
    private bool dirOpposite;

    private float changeDir = 1;
    private bool maxY = false;

    private Player player;
    private Vector3 movingSaw;
    private Animator animator;

    private void Start()
    {
        if (maxPosY == 0)
        {
            animator = GetComponent<Animator>();
        }

        player = Player.MyInstance;
        movingSaw = this.transform.position;
    }

    private void FixedUpdate()
    {
        if (maxPosY == 0)
        {
            movingSaw.x += speedX * changeDir;
            movingSaw.y += speedY * changeDir;
        }
        else
        {
            if (!dirOpposite)
            {
                if (player.IsAlive)
                {
                    if (Vector2.Distance(player.transform.position, transform.position) <= range && this.transform.position.y < maxPosY && !maxY)
                    {
                        movingSaw.y += speedY;
                    }
                    else
                    {
                        maxY = true;
                        MoveDown();
                    }
                }
                else
                {
                    MoveDown();
                }
            }
            else
            {
                if (player.IsAlive)
                {
                    if (Vector2.Distance(player.transform.position, transform.position) <= range && this.transform.position.y > maxPosY && !maxY)
                    {
                        movingSaw.y -= speedY;
                    }
                    else
                    {
                        maxY = true;
                        MoveUp();
                    }
                }
                else
                {
                    MoveUp();
                }
            }
        }

        this.transform.position = movingSaw;
    }

    private void MoveDown()
    {
        if (this.transform.position.y > minPosY)
        {
            movingSaw.y -= speedY;
        }
        else
        {
            maxY = false;
        }
    }

    private void MoveUp()
    {
        if (this.transform.position.y < minPosY)
        {
            movingSaw.y += speedY;
        }
        else
        {
            maxY = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && maxPosY == 0)
        {
            animator.SetTrigger("Blood");
        }

        if (collision.tag == "Saw")
        {
            changeDir *= -1;
        }
    }
}
