﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private Transform exitPoint;
    private LineRenderer lineRenderer;
    private Quaternion rotation;
    private Player player;

    private void Start()
    {
        rotation = this.transform.parent.parent.rotation;
        exitPoint = this.transform.parent;
        lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.enabled = false;
        lineRenderer.useWorldSpace = true;
    }

    private void LateUpdate()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up);
        Debug.DrawRay(transform.position, hit.point);

        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, hit.point);

        if (hit.collider.tag == "Player" && lineRenderer.enabled)
        {
            player = hit.collider.gameObject.GetComponent<Player>();
            this.transform.parent.parent.rotation = rotation;
            lineRenderer.enabled = false;

            /*player.MyDeaths++;
            player.Death();*/
        }

        if (player == null) player = Player.MyInstance;
        if (!player.IsAlive)
        {
            var line = FindObjectsOfType<LineRenderer>();
            foreach (var l in line)
            {
                l.enabled = false;
            }
        }
    }
}