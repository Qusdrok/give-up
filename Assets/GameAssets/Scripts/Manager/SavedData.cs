﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveData
{
    public PlayerData MyPlayerData { get; set; }
    public List<HighScoreData> MyHighScoreData { get; set; }

    public SaveData()
    {
        MyHighScoreData = new List<HighScoreData>();
    }
}

[Serializable]
public class PlayerData
{
    public int MyDeath { get; set; }
    public float MyMiliSeconds { get; set; }
    public float MySeconds { get; set; }
    public float MyMinutes { get; set; }
    public float MyHours { get; set; }
    public bool HasRemoveAds { get; set; }

    public PlayerData(int death, float miliseconds, float seconds, float minutes, float hours, bool hasRemoveAds)
    {
        this.MyDeath = death;
        this.MyMiliSeconds = miliseconds;
        this.MySeconds = seconds;
        this.MyMinutes = minutes;
        this.MyHours = hours;
        this.HasRemoveAds = hasRemoveAds;
    }
}

[Serializable]
public class HighScoreData
{
    public string MyName { get; set; }
    public string MyTimer { get; set; }
    public string MyFloor { get; set; }
    public string MyDeaths { get; set; }

    public HighScoreData(string name, string timer, string floor, string deaths)
    {
        this.MyName = name;
        this.MyTimer = timer;
        this.MyFloor = floor;
        this.MyDeaths = deaths;
    }
}