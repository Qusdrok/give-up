﻿using System.Collections;
using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    public static PlatformManager MyInstance;

    private void Awake()
    {
        if (MyInstance == null) MyInstance = this;
    }

    public IEnumerator SpawnPlatform(Vector2 spawn)
    {
        yield return new WaitForSeconds(2.5f);
        SpawnerHelper.CreateSpawner(spawn, null, 0);
    }
}
