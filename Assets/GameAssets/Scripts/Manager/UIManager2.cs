﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager2 : MonoBehaviour
{
    [SerializeField]
    private Player player;

    [SerializeField]
    private GameObject mainFrame, sound;

    [SerializeField]
    private TextMeshProUGUI timer, done;

    [SerializeField]
    private Sprite soundOff, soundOn, levelMarkerComplete;

    [SerializeField]
    private GameObject[] levelMarker;

    public static UIManager2 MyInstance;
    private Image soundImage;
    private Vector3 direction;
    private float alpha;

    private void Awake()
    {
        if (MyInstance == null) MyInstance = this;
    }

    private void Start()
    {
        
        player.UpdateTimer();

        float height = Camera.main.orthographicSize / 1.8f;
        float width = height * Screen.width / Screen.height;

        mainFrame.transform.localScale = new Vector3(height, width);
        soundImage = sound.GetComponent<Image>();
    }

    private void Update()
    {
        if (player.MyMiliseconds >= 99)
        {
            if (player.MySeconds >= 59)
            {
                player.MyMinutes++;
                player.MySeconds = 0;
            }

            if (player.MyMinutes >= 59)
            {
                player.MyHours++;
                player.MyMinutes = 0;
            }

            player.MySeconds++;
            player.MyMiliseconds = 0;
        }

        player.MyMiliseconds += Time.deltaTime * 100;
        timer.text = string.Format("{0}:{1}:{2}:{3}", player.MyHours, player.MyMinutes, player.MySeconds, (int)player.MyMiliseconds);
    }

    public void OpenClose(CanvasGroup canvasGroup)
    {
        alpha = canvasGroup.alpha = canvasGroup.alpha > 0 ? 0 : 1;
        canvasGroup.blocksRaycasts = canvasGroup.blocksRaycasts == true ? false : true;
    }

    public void TimeStop()
    {
        Time.timeScale = (alpha == 1) ? 0 : 1;
    }

    public void Sound(Image sound)
    {
        if (sound.sprite.name.Contains("890"))
        {
            sound.sprite = soundOff;
            SoundManager.StopSound();
        }
        else
        {
            SoundManager.PlaySound();
            sound.sprite = soundOn;
        }
    }

    public void CompleteLevel(int level)
    {
        int amount = 0;
        float percent = level * (100f / levelMarker.Length);
        done.text = string.Format("{0:#.0} % Done", percent);

        for (int i = 0; i < levelMarker.Length; i++)
        {
            if (amount < level)
            {
                levelMarker[i].GetComponent<Image>().sprite = levelMarkerComplete;
                amount++;
            }
        }
    }
}
