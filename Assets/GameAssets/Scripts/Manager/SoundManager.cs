﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static SoundManager instance;
    private static AudioClip mainSound;
    private static AudioSource audioSource;

    private static bool enableSound = true;
    public static bool MyEnableSound { get => enableSound; set => enableSound = value; }

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        mainSound = Resources.Load<AudioClip>("mainSound");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound()
    {
        audioSource.clip = mainSound;
        enableSound = true;
        PlayerPrefs.SetString("Sound", "true");

        audioSource.Play();
    }

    public static void StopSound()
    {
        enableSound = false;
        PlayerPrefs.SetString("Sound", "false");

        audioSource.Stop();
    }

    public static bool Playing()
    {
        return audioSource.isPlaying;
    }
}
