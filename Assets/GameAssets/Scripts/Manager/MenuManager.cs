﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private Player player;

    [SerializeField]
    private CanvasGroup dialogue, menu, enterName, requestUpdate;

    [SerializeField]
    private GameObject highScoreArea, highscorePrefab, ProductRemoveAds;

    [SerializeField]
    private TextMeshProUGUI nameText;
    

    [SerializeField]
    private string latestVersion, url;
    

    private void Awake()
    {
        if (player.RemoveAds)
        {
            Destroy(ProductRemoveAds);
        }
    }

    public void OpenClose(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = canvasGroup.alpha > 0 ? 0 : 1;
        canvasGroup.blocksRaycasts = canvasGroup.blocksRaycasts == true ? false : true;
    }

    public void HighScore(CanvasGroup highscore)
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            OpenClose(highscore);
            OpenClose(enterName);

            PlayerPrefs.DeleteKey("HighScore");
        }
        else
        {
            OpenClose(menu);
        }
    }

    public void DisableEnterName()
    {
        enterName.alpha = 0;
        enterName.blocksRaycasts = false;
    }

    public void Play()
    {
        if (PlayerPrefs.HasKey("Load"))
        {
            OpenClose(dialogue);
        }
        else
        {
            NewGame();
        }
    }

    public void Continue()
    {
        SceneManager.LoadScene("Level1");
    }

    public void NewGame()
    {
        PlayerPrefs.DeleteKey("Load");
        PlayerPrefs.DeleteKey("BonusLevel0");
        PlayerPrefs.DeleteKey("BonusLevel1");
        PlayerPrefs.DeleteKey("BonusLevel2");

        SceneManager.LoadScene("Level1");
    }

    public void OpenURL()
    {
        Application.OpenURL(url);
    }
}
