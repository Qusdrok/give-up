﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ViewManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI description;

    [SerializeField]
    private string[] title;

    [SerializeField]
    private float fadeTime;

    private int i = 0;

    private void Start()
    {
        Time.timeScale = 1;
        EffectEndGame(i);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
        PlayerPrefs.SetInt("HighScore", 1);
    }

    private async void EffectEndGame(int i)
    {
        if (i < title.Length)
        {
            description.text = title[i];
            description.color = Color.white;

            float rate = 1.0f / fadeTime;
            float progress = 0.0f;

            while (progress < 1.0f)
            {
                description.color = Color.Lerp(Color.white, Color.clear, progress);
                progress += rate * Time.deltaTime;

                await Task.Yield();
            }

            description.color = Color.clear;
            EffectEndGame(++i);
        }
        else
        {
            LoadMenu();
        }
    }
}
