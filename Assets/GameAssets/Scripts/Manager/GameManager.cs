﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject openDoor, nextLevelDoor, eye;

    [SerializeField] private Player player;

    [SerializeField] private float range;

    [SerializeField] private Vector3 center;

    [SerializeField] private float radius;

    public static GameManager MyInstance;
    private Vector3 direction;
    private bool isOpen = false;

    private void Awake()
    {
        if (MyInstance == null) MyInstance = this;
    }

    private void FixedUpdate()
    {
        direction = (player.transform.position - eye.transform.position).normalized;
        if ((eye.transform.position - player.transform.position).magnitude > 0.1f)
            eye.transform.Translate(direction * Time.deltaTime);

        Vector3 pos = eye.transform.position;
        Vector3 offset = pos - center;
        eye.transform.position = center + Vector3.ClampMagnitude(offset, radius);
    }
}