﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField]
    private GameObject exitPoint;

    [SerializeField]
    private float range;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float speedBullet;

    [SerializeField]
    private float timeDelay;

    [SerializeField]
    private bool laser;

    private float shootDelay;
    private Quaternion rotation;
    private Player player;

    private void Start()
    {
        rotation = this.transform.rotation;
        player = Player.MyInstance;
    }

    private void FixedUpdate()
    {
        if (player.IsAlive)
        {
            if (Vector2.Distance(player.transform.position, transform.position) <= range)
            {
                shootDelay += Time.deltaTime;

                Vector3 target = transform.position - player.transform.position;
                float angle = Mathf.Atan2(target.y, target.x) * Mathf.Rad2Deg;

                Quaternion quaternion = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, quaternion, Time.deltaTime * speed);

                if (shootDelay >= timeDelay && !laser)
                {
                    shootDelay = 1;

                    Vector3 direction = player.transform.position - transform.position;
                    GameObject go = SpawnerHelper.CreateSpawner(exitPoint.transform.position, null, 1);
                    go.GetComponent<Rigidbody2D>().velocity = direction * speedBullet;
                }
                else if (laser)
                {
                    this.GetComponentInChildren<LineRenderer>().enabled = true;
                }
            }
            else
            {
                if (laser)
                {
                    this.GetComponentInChildren<LineRenderer>().enabled = false;
                }
            }
        }
        else
        {
            this.transform.rotation = rotation;
        }
    }
}
