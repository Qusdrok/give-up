﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BonusLevel : MonoBehaviour
{
    [SerializeField]
    private Sprite unlock, incomplete, complete;

    [SerializeField]
    private int requestLevel, index;

    public bool IsLevelComplete { get; set; }

    private Image image;
    private Button bonusLevel;

    private void Start()
    {
        image = this.GetComponent<Image>();
        bonusLevel = this.GetComponent<Button>();
        bonusLevel.interactable = false;

        string BonusLevel = PlayerPrefs.GetString("BonusLevel" + index);
        if (PlayerPrefs.GetInt("Load") >= requestLevel && !BonusLevel.Contains("plete"))
        {
            image.sprite = unlock;
            bonusLevel.interactable = true;
        }
        else if (BonusLevel == "Incomplete")
        {
            image.sprite = incomplete;
            bonusLevel.interactable = true;
        }
        else if (BonusLevel == "Completed")
        {
            image.sprite = complete;
            bonusLevel.interactable = true;
        }
    }

    public void Incomplete(GameObject action)
    {
        SceneManager.LoadScene(action.name);

        if (!PlayerPrefs.HasKey("BonusLevel" + index))
        {
            PlayerPrefs.SetString("BonusLevel" + index, "Incomplete");
            image.sprite = incomplete;
        }
    }
}
