﻿using System.Collections;
using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Enum;
using UnityEngine;

public class ActionGame : MonoBehaviour
{
    [SerializeField]
    private bool rotation, run, sinodialMove;

    [SerializeField]
    private float delayAnimation;

    [SerializeField]
    private float speed = 2.5f;

    [SerializeField]
    private float frequency = 20f;

    [SerializeField]
    private float magnitude = 0.5f;

    [SerializeField]
    private float minPos, maxPos;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private PolygonCollider2D polygonCollider2D;

    [SerializeField]
    private bool facingRight = true;

    private bool facing;
    private Vector3 pos, scale, localPos, localScale;
    private Player player;

    private void Start()
    {
        player = Player.MyInstance;
        localPos = pos = this.transform.position;
        localScale = scale = this.transform.localScale;

        facing = facingRight;
    }

    private void FixedUpdate()
    {
        if (rotation)
        {
            Rotation();
        }
        else if (run)
        {
            Run();
        }
        else if (sinodialMove)
        {
            MoveSinodial();
        }
    }

    private void Rotation()
    {
        transform.Rotate(new Vector3(0, 0, 200) * Time.deltaTime);
    }

    private void Run()
    {
        if (player.IsCharacterState(CharacterState.Move))
        {
            animator.enabled = true;
            CheckWhereToFace();

            if (facingRight)
            {
                pos += this.transform.right * Time.deltaTime * speed;
            }
            else
            {
                pos -= this.transform.right * Time.deltaTime * speed;
            }

            this.transform.position = pos;
        }
        else
        {
            this.transform.position = pos = localPos;
            this.transform.localScale = scale = localScale;

            facingRight = facing;
            animator.enabled = false;
        }
    }

    private void MoveSinodial()
    {
        CheckWhereToFace();
        if (!facingRight)
        {
            MoveRight();
        }
        else
        {
            MoveLeft();
        }
    }

    private void CheckWhereToFace()
    {
        if (pos.x < minPos)
        {
            facingRight = !facingRight;
        }
        else if (pos.x > maxPos)
        {
            facingRight = !facingRight;
        }

        if ((facingRight && (scale.x < 0)) || (!facingRight && (scale.x > 0)))
        {
            scale.x *= -1;
        }

        this.transform.localScale = scale;
    }

    private void MoveRight()
    {
        pos += this.transform.right * Time.deltaTime * speed;
        this.transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }

    private void MoveLeft()
    {
        pos -= this.transform.right * Time.deltaTime * speed;
        this.transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }

    public IEnumerator WaitAnimator()
    {
        animator.enabled = false;
        polygonCollider2D.enabled = false;
        yield return new WaitForSeconds(4f);

        animator.enabled = true;
        polygonCollider2D.enabled = true;
    }
}
