﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformHidden : MonoBehaviour
{
    [SerializeField]
    private float range;

    private Player player;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        player = Player.MyInstance;
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }

    private void FixedUpdate()
    {
        this.spriteRenderer.enabled = (Vector2.Distance(player.transform.position, transform.position) <= range) ? true : false;
    }
}
