﻿using System;
using System.Collections;
using System.Threading.Tasks;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

public class Platform : MonoBehaviour
{
    private Rigidbody2D RigidBody;
    private BoxCollider2D BoxCollider;

    private void Start()
    {
        RigidBody = this.GetComponent<Rigidbody2D>();
        BoxCollider = this.GetComponent<BoxCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            Player player = collision.collider.GetComponent<Player>();
            if (player.IsGround() && player.IsAlive)
            {
                PlatformManager.MyInstance.StartCoroutine("SpawnPlatform", 
                    new Vector2(transform.position.x, transform.position.y));
                DestroyPlatform(0.5f);
            }
        }
    }

    private void DropPlatform()
    {
        RigidBody.isKinematic = false;
    }

    private async void DestroyPlatform(float delay)
    {
        Invoke("DropPlatform", 0.5f);
        await Task.Delay(TimeSpan.FromSeconds(delay));

        SpawnerHelper.DestroySpawner(this.gameObject);
        RigidBody.isKinematic = true;
    }
}
