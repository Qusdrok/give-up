﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
    [SerializeField]
    private Sprite[] transitions;

    [SerializeField]
    private GameObject[] levels;

    [SerializeField]
    private Transform parentLevel, level1;

    [SerializeField]
    private float transitionSpeed = 2f;

    public static SplashScreen MyInstance;
    private Image image;

    private void Awake()
    {
        if (MyInstance == null) MyInstance = this;
    }

    private void Start()
    {
        image = GetComponent<Image>();
        if (PlayerPrefs.GetInt("Load") > 1)
        {
            int index = PlayerPrefs.GetInt("Load") - 1;
            GameObject newLevel = Instantiate(levels[index], parentLevel);
            level1.gameObject.SetActive(false);

            /*Player.MyInstance.SetFloorAndRespawnPosition("Floor" + (index + 1), newLevel.transform.GetChild(0));
            UIManager2.MyInstance.CompleteLevel(index + 1);
            GameManager.MyInstance.SetObjectOpenAndNextLevel(newLevel.transform.GetChild(0).gameObject, 
                newLevel.transform.GetChild(2).gameObject);*/
        }
        else
        {
            UIManager2.MyInstance.CompleteLevel(1);
        }
    }

    public async void SceneTransition(int index)
    {
        UIManager2.MyInstance.OpenClose(this.GetComponent<CanvasGroup>());
        /*Player.MyInstance.SetFloorAndRespawnPosition("Floor" + (index + 1), levels[index].transform.GetChild(0));
        SwitchTransition();

        while (image.material.GetFloat("_Progress") > (-0.1f - image.material.GetFloat("_EdgeSmoothing")))
        {
            image.material.SetFloat("_Progress", Mathf.MoveTowards(image.material.GetFloat("_Progress"),
                    -0.1f - image.material.GetFloat("_EdgeSmoothing"), transitionSpeed * Time.deltaTime));
            await Task.Yield();
        }

        await Task.Delay(TimeSpan.FromSeconds(1.5f));
        GameObject newLevel = Instantiate(levels[index], parentLevel);
        GameManager.MyInstance.SetObjectOpenAndNextLevel(newLevel.transform.GetChild(0).gameObject, 
            newLevel.transform.GetChild(2).gameObject);*/

        SwitchTransition();
        while (image.material.GetFloat("_Progress") < 1.1f)
        {
            image.material.SetFloat("_Progress", Mathf.MoveTowards(image.material.GetFloat("_Progress"),
                    1.1f, transitionSpeed * Time.deltaTime));
            await Task.Yield();
        }

        Player.MyInstance.CompleteNewLevel(index);
        UIManager2.MyInstance.OpenClose(this.GetComponent<CanvasGroup>());
    }

    public void SwitchTransition()
    {
        int newTransition = UnityEngine.Random.Range(0, transitions.Length);
        image.material.SetTexture("_TransitionEffect", transitions[newTransition].texture);
    }
}
