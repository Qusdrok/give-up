﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public abstract class Character : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    protected float jumping = 20f;

    [SerializeField]
    protected bool isAlive;

    [SerializeField]
    private Rigidbody2D myRigidBody;

    [SerializeField]
    private Animator myAnimator;

    [SerializeField]
    private LayerMask layerMask;

    private Vector2 direction;
    public Vector2 Direction { get => direction; set => direction = value; }

    public PolygonCollider2D MyPolygonCollider2D { get; set; }
    public Animator MyAnimator { get => myAnimator; }
    public Rigidbody2D MyRigidBody { get => myRigidBody; }

    protected bool doubleJump = false;
    public bool IsAlive { get => isAlive; set => isAlive = value; }
    public int MyDeaths { get; set; }
    public bool isGround { get; set; }
    public bool isJumping { get; set; }
    public bool isMoving { get; set; }
    public float MySpeed { get => speed; }

    protected virtual void Start()
    {
        isMoving = false;
        MyPolygonCollider2D = GetComponent<PolygonCollider2D>();
    }

    protected virtual void Update()
    {
        IsGround();
        HandleJumpAndFall();
    }

    private void HandleJumpAndFall()
    {
        if (isJumping && IsAlive)
        {
            if (MyRigidBody.velocity.y > 0)
            {
                MyAnimator.SetInteger("State", 3);
            }
            else
            {
                MyAnimator.SetInteger("State", 1);
            }
        }
    }

    public void Idle()
    {
        MyAnimator.SetInteger("State", 0);
    }

    public bool IsGround()
    {
        RaycastHit2D hit = Physics2D.BoxCast(MyPolygonCollider2D.bounds.center, MyPolygonCollider2D.bounds.size, 0f, Vector2.down, 0.5f, layerMask);
        return hit.collider != null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isJumping = false;
            doubleJump = false;

            if (IsAlive)
            {
                Idle();
            }
        }
    }
}
