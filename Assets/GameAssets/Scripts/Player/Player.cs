﻿using System.Collections;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay;
using GameAssets.Scripts.GameBase.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Player : BaseCharacter
{
    [SerializeField]
    private Transform respawn;

    [SerializeField]
    private SpriteRenderer playerIcon;

    [SerializeField]
    private TextMeshProUGUI deaths, timer, floor;

    [SerializeField]
    private Joystick joystick;

    [SerializeField]
    private float fadeTime;

    [SerializeField]
    private bool bonusLevel;

    public static Player MyInstance;
    private Vector3 min, max;
    private Vector2 initPos;
    private bool facingRight;

    private float miliseconds;
    private float seconds;
    private float minutes;
    private float hours;

    public float MyMiliseconds { get => miliseconds; set => miliseconds = value; }
    public float MySeconds { get => seconds; set => seconds = value; }
    public float MyMinutes { get => minutes; set => minutes = value; }
    public float MyHours { get => hours; set => hours = value; }
    public bool MyBonusLevel { get => bonusLevel; }

    public bool RemoveAds { get; set; }

    private void Awake()
    {
        if (MyInstance == null) MyInstance = this;
    }

    protected override void Start()
    {
        RemoveAds = false;
        facingRight = true;
        base.Start();
    }

    public void UpdateTimer()
    {
        timer.text = $"{MyHours}:{MyMinutes}:{MySeconds}:{MyMiliseconds}";
    }

    public IEnumerator EffectLoadScene(int index)
    {
        playerIcon.color = Color.white;

        float rate = 1.0f / fadeTime;
        float progress = 0.0f;

        while (progress < 1.0f)
        {
            playerIcon.color = Color.Lerp(Color.white, Color.clear, progress);
            progress += rate * Time.deltaTime;

            yield return null;
        }

        playerIcon.color = Color.clear;
        int random = Random.Range(0, 100);

        if (random < 55)
        {
            if (bonusLevel)
            {
                string scene = SceneManager.GetActiveScene().name;
                switch (scene)
                {
                    case "BonusA": PlayerPrefs.SetString("BonusLevel0", "Completed"); break;
                    case "BonusB": PlayerPrefs.SetString("BonusLevel1", "Completed"); break;
                    case "BonusC": PlayerPrefs.SetString("BonusLevel2", "Completed"); break;
                }

                SceneManager.LoadScene(index);
            }
            else
            {
                if (index == 40)
                {
                    SceneManager.LoadScene("Won");
                }
                else
                {
                    SplashScreen.MyInstance.SceneTransition(index);
                }
            }
        }
        else
        {
            AdsManager.ShowAds();
            SplashScreen.MyInstance.SceneTransition(index);
        }
    }


    public void CompleteNewLevel(int index)
    {
        playerIcon.color = Color.white;
        UIManager2.MyInstance.CompleteLevel(index + 1);
    }
}
